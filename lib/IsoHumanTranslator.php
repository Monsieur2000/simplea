<?php
namespace Lib;

use DateTime;
use Locale;

class IsoHumanTranslator
{
    public $langue;

    public function __construct($langue="fr_FR")
    {
        $this->langue = $langue;
        setlocale(LC_ALL, "fr_CH");
    }

    public function isoDatesToHuman($dates)
    {
        foreach ($dates as $date) {
            $date = explode('/',$date);

            foreach ($date as &$d) {
                $timestamp = strtotime($d);
                switch (strlen($d)) {
                    case 7:
                      $d = utf8_encode(strftime('%B %Y',$timestamp));
                      break;
                    case 10:
                      $d = utf8_encode(strftime('%e %B %Y',$timestamp));
                      break;
                  }
            }
            unset($d);
            $human[] = implode(' - ', $date);
        }
        return $human;
    }

    public function isoLanguesToHuman($langues)
    {
        foreach ($langues as &$langue) {
            $langue = Locale::getDisplayLanguage($langue);
        }
        return $langues;
    }
}
