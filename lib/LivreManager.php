<?php
namespace Lib;

use MongoDB;

class LivreManager
{
    private $manager;
    private $namespace;

    public function __construct($uri, $namespace)
    {
      $this->manager = new MongoDB\Driver\Manager($uri);
      $this->namespace = $namespace;
    }

    public function getLivre($id)
    {
        if(!isset($id) || strlen($id) != 24 || !ctype_alnum($id)) return false;
        $id = new MongoDB\BSON\ObjectID($id);
        $filtre = ['_id' => $id];
        $query = new MongoDB\Driver\Query($filtre);
        $result = $this->manager->executeQuery($this->namespace, $query)->toArray();
        if(count($result) > 0) return $result[0];
        else return false;

    }
}
