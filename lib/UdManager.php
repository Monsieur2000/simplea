<?php
namespace Lib;

use MongoDB;

class UdManager
{
    private $manager;
    private $namespace;

    public function __construct($uri, $namespace)
    {
      $this->manager = new MongoDB\Driver\Manager($uri);
      $this->namespace = $namespace;
    }

    public function getUd($id)
    {
        if(!isset($id)) return false;
        $filtre = ['_id' => $id];
        $query = new MongoDB\Driver\Query($filtre);
        $result = $this->manager->executeQuery($this->namespace, $query)->toArray();
        if(count($result) > 0) return $result[0];
        else return false;
    }

    public function getParentsData($ids)
    {
      $parents = null;
      if (!isset($ids)) return $parents;

      $query = new MongoDB\Driver\Query(['_id'=> ['$in' => $ids]]);

      $cursor = $this->manager->executeQuery($this->namespace, $query);
      foreach ($cursor as $ud) {
        $impmat = null;
	foreach ($ud->impmat as $m) {
		if(preg_match('/[Pp]ièce?/', $m) === 1) $impmat = $m;
        }

        $parents[] = array(
          'id' => $ud->_id,
          'titre' => $ud->titre,
          'cote' => $ud->cotes[0]->val,
          'impmat' => $impmat,
          'dates' => $this->getAnneesFromDate($ud->dates)
         );
      }
      // $parents = array_reverse($parents);
      return $parents;
    }

    public function getAnneesFromDate($dates)
    {
      if (!isset($dates)) return ['[s.d.]'];

      foreach ($dates as $date) {
        $date = explode('/',$date);

        foreach ($date as &$d) {
          $d = substr($d,0,4);
        }
        unset($d);
        $annees[] = implode('-', $date);
      }
      return $annees;
    }

    public function getCotesDispo($niv_acces=0)
    {
        $query = new MongoDB\Driver\Query(['docs_num.chemin' => ['$regex' => "diffusion"],'docs_num_acces' => ['$lt' => $niv_acces]],['projection' => ['_id']]);
        $cursor = $this->manager->executeQuery($this->namespace, $query);

        return $cursor->toArray();
    }

    public function getFonds()
    {
        $query = new MongoDB\Driver\Query(['niveau' => 'fonds'],['sort' => ['vedette' => 1]]);
        $cursor = $this->manager->executeQuery($this->namespace, $query);

        $fonds = array();
        foreach ($cursor as $ud) {

            $fonds[] = array(
                'radical' => $ud->_id,
                'cote' => $ud->cotes[0]->val,
                'titre' => $ud->titre,
                'dates' => $this->getAnneesFromDate($ud->dates)
            );
        }

        return $fonds;
    }

}
