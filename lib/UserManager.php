<?php
namespace Lib;

use MongoDB;

class UserManager
{
    private $manager;
    private $namespace;

    public function __construct($uri, $namespace)
    {
      $this->manager = new MongoDB\Driver\Manager($uri);
      $this->namespace = $namespace;
    }

    private function fetchUser($key)
    {
        if(!ctype_alnum($key) || !isset($key)) return array();
        $today = new MongoDB\BSON\UTCDateTime();
        $filtre = ['clef' => $key, 'expiration' => ['$gte' => $today]];
        $query = new MongoDB\Driver\Query($filtre);
        return $this->manager->executeQuery($this->namespace, $query)->toArray();
    }

    public function checkKey($key)
    {
        $result = $this->fetchUser($key);
        if(count($result) > 0) return true;
        else return false;
    }

    public function groupId($key=null)
    {
        $groupe = 0;
        if(!isset($key) && isset($_COOKIE['ukey'])) $key = $_COOKIE['ukey'];
        $result = $this->fetchUser($key);
        if(count($result) > 0) $groupe = $result[0]->groupe_id;

        return $groupe;
    }

    public function userInfo($key=null)
    {
        $infos = false;
        if(!isset($key) && isset($_COOKIE['ukey'])) $key = $_COOKIE['ukey'];
        $result = $this->fetchUser($key);
        if(count($result) > 0) $infos = $result[0];

        return $infos;
    }

    public function genCookies($key)
    {
        $result = $this->fetchUser($key);

        if(count($result) < 1){return false;}
        setcookie('ukey', $key, intval((string) $result[0]->expiration)/1000, '/', $_SERVER['SERVER_NAME'], false, true);
    }



    public function randomKey($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $pieces = [];
        $max = strlen($keyspace) - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

    function encryptCookie($value){
        if(!$value){return false;}
        $key = 'The Line Secret Key';
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $text, MCRYPT_MODE_ECB, $iv);
        return trim(base64_encode($crypttext)); //encode for cookie
    }

    function decryptCookie($value){
        if(!$value){return false;}
        $key = 'The Line Secret Key';
        $crypttext = base64_decode($value); //decode cookie
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

}
