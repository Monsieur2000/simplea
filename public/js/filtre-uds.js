function filtreUds() {
  // Declare variables
  var input, filter, table, tr, td, i;
  input = document.getElementById("filtre-input");
  filter = input.value.toUpperCase();
  table = document.getElementById("infofonds").getElementsByTagName("tbody")[0];
  tr = table.getElementsByClassName("sort");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    cote = tr[i].getElementsByTagName("td")[0];
    titre = tr[i].getElementsByTagName("td")[1].getElementsByTagName("a")[0];
    //console.log(cote.innerHTML)
    if (titre.innerHTML.toUpperCase().indexOf(filter) > -1 || cote.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
    } else {
        tr[i].style.display = "none";
    }

  }
}
