<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path'], $settings['template_variables']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// udManager
$container['udManager'] = function ($c) {
    $settings = $c->get('settings')['mongodb'];
    $udManager = new Lib\UdManager($settings['uri'], $settings['ns_uds']);
    return $udManager;
};

$container['livreManager'] = function ($c) {
    $settings = $c->get('settings')['mongodb'];
    $livreManager = new Lib\LivreManager($settings['uri'], $settings['ns_livres']);
    return $livreManager;
};

$container['userManager'] = function ($c) {
    $settings = $c->get('settings')['mongodb'];
    $userManager = new Lib\UserManager($settings['uri'], $settings['ns_users']);
    return $userManager;
};

$container['isoManager'] = function ($c) {
    $isoManager = new Lib\IsoHumanTranslator();
    return $isoManager;
};

$container['parsedown'] = function ($c) {
    $parsedown = new Parsedown();
    $parsedown->setBreaksEnabled(true);
    return $parsedown;
};

// Solr
$container['searcher'] = function ($c) {
    $settings = $c->get('settings')['solr'];
    $searcher = new Lib\SolrModel($settings);
    return $searcher;
};

// Pour afficher une page 404 perso FIXME : la template a les url des document en dur
$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['renderer']->render($response->withStatus(404), '404.phtml');
    };
};
