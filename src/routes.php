<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

//$app->redirect('/', '/angstrom/listfonds', 301);

$app->get('/', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'home.phtml', $args);
});

$app->get('/archives/{id}[/{typevue}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    //$this->logger->info("Slim-Skeleton '/' route");

    // récupération des infos de l'ud
    $ud = $this->udManager->getUd($args['id']);
    if($ud === false) throw new \Slim\Exception\NotFoundException($request, $response);
    //$args['ud'] = json_encode($this->udManager->getUd($filtre), JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);

    // ajout info et mise en forme des données.
    $ud->fonds = $this->udManager->getParentsData([$ud->fonds]);
    $ud->dates = $this->isoManager->isoDatesToHuman($ud->dates);
    $ud->langues = $this->isoManager->isoLanguesToHuman($ud->langues);

    // arguments pour les urls et l'affichage du text des liens vers les pages fonds
    $args['btn_bread']['label'] = 'Information sur le fonds';
    $args['btn_bread']['args_url'] = array ('id' => $ud->fonds[0]['id']);
    $args['miette_fonds_bread']['args_url'] = array('id' => $ud->fonds[0]['id'], 'typevue' => 's');

    $args['router'] = $this->router;
    $args['parsedown'] = $this->parsedown;
    $args['ud'] = $ud;

    // choix du type de page (fonds, intermédiaire, document) en fonction de la présence des enfants et parents
    switch([isset($ud->parents), isset($ud->enfants)]) {
        case [false, true]:
        case [false, false] :
            //niveau 'fonds';
            $args['niveau_piece'] = false;
            $ud->parents = []; // pour éviter l'erreur dans le forech des breadcrumbs

            //choix du type de vue du fonds
            if ($args['typevue'] == 's') {
                // s pour vue des sous-unités

                // ajout info et mise en forme des données.
                $ud->enfants = $this->udManager->getParentsData($ud->enfants);
                // générer la template list_enfants
                $args['article'] = $this->renderer->fetch('list_enfants.phtml', $args);
                $args['titre_head'] = $ud->fonds[0]['titre']." - contenu";
            } else {
                // générer la template info_ud
                $args['btn_bread']['label'] = 'Naviguer dans le fonds';
                $args['btn_bread']['args_url'] = array ('id' => $ud->fonds[0]['id'], 'typevue' => 's');
                $args['href_fonds_bread']['args_url'] = array('id' => $ud->fonds[0]['id']);

                $args['article'] = $this->renderer->fetch('info_ud.phtml', $args);
                $args['titre_head'] = $ud->fonds[0]['titre']." - détail";
            }
            break;
        case [true, true]:
            //niveau 'intermédiaire';
            $args['niveau_piece'] = false;
            // ajout info et mise en forme des données.
            $args['btn_navud']['label'] = 'Information sur le dossier';
            $args['btn_navud']['args_url'] = array ('id' => $ud->_id, 'typevue' => 'i');

            // On enlève la première ligne qui est l'id du fonds et qui prose problème dans l'ordre de retour des documents
            $ud->parents = array_slice($ud->parents, 1);
            $ud->parents = $this->udManager->getParentsData($ud->parents);

            //choix du type de vue du dossier
            if ($args['typevue'] == 'i') {
                // i pour vue info

                $args['btn_navud']['label'] = 'Naviguer dans le dossier';
                $args['btn_navud']['args_url'] = array ('id' => $ud->_id);
                $args['article'] = $this->renderer->fetch('info_ud.phtml', $args);
                $args['titre_head'] = $ud->fonds[0]['titre']." : ".$ud->titre." - détail";
            } else {
                $ud->enfants = $this->udManager->getParentsData($ud->enfants);
                $args['article'] = $this->renderer->fetch('list_enfants.phtml', $args);
                $args['titre_head'] = $ud->fonds[0]['titre']." : ".$ud->titre." - contenu";
            }

            $args['nav_ud'] = $this->renderer->fetch('nav_ud.phtml', $args);
            break;
        case [true, false]:
            //niveau 'document';
            $args['niveau_piece'] = true;

            $args['niv_acces'] = $this->userManager->groupId();

            // ajout info et mise en forme des données.
            $ud->parents = array_slice($ud->parents, 1); // On enlève la première ligne qui est l'id du fonds et qui prose problème dans l'ordre de retour des infos
            $ud->parents = $this->udManager->getParentsData($ud->parents);
            $args['nav_ud'] = $this->renderer->fetch('nav_ud.phtml', $args);
            $args['article'] = $this->renderer->fetch('info_ud.phtml', $args);
            $args['titre_head'] = $ud->fonds[0]['titre']." : ".$ud->titre." (".implode(', ',$ud->dates).")";
            // générer la template info_ud
            break;
    }

    $args['bread'] = $this->renderer->fetch('breadcrumbs.phtml', $args);
    $args['scripts_js'] = array('infofonds-sorttable', 'filtre-uds', 'w3-slideshow');

    return $this->renderer->render($response, 'cadre.phtml', $args);

})->setName('ud-detail');

$app->get('/search', function (Request $request, Response $response, array $args) {
    $requete = trim($request->getQueryParam('q'));
    $requete = urldecode($requete);
    $filtres = array();
    $args['titre_head'] = "$requete - Recherche archives FJME";
    $args['niveau_piece'] = false;
    $args['vue_mosaique'] = 0;
    $template = 'resultats_table.phtml';

    if($request->getQueryParam('vue_mosaique') !== null) {
        $args['vue_mosaique'] = $request->getQueryParam('vue_mosaique');
    }

    if($args['vue_mosaique'] == 1) $template = 'resultats_img.phtml';

    // Calcul de la fourchette de résultat à afficher en fonction du numéro de la page
    // la plage s'exprime ainis : (ligne de départ, nombre de résultats à afficher)
    if($request->getQueryParam('page') !== null) {
      $page = $request->getQueryParam('page');
      $plage_nb_results = array((50*($page-1)),50);
    } else {
      $page = 1;
      $plage_nb_results = array(0,50);
    }

    // Ajout des filtres de facettes
    if($request->getQueryParam('vedette') !== null) {
        $filtres['vedette'] = $request->getQueryParam('vedette');
    }
    if($request->getQueryParam('sourceType') !== null) {
        $filtres['sourceType'] = $request->getQueryParam('sourceType');
    }
    if($request->getQueryParam('auteur') !== null) {
        $filtres['auteur'] = $request->getQueryParam('auteur');
    }
    if($request->getQueryParam('fonds') !== null) {
        $filtres['fonds'] = $request->getQueryParam('fonds');
    }
    // Requête
    $result = $this->searcher->search($requete,$filtres,$plage_nb_results);

    // Pagination
    $args['nb_pages'] = ceil($result->response->numFound/50);
    $args['page'] = $page;

    //Document numériques disponibles
    $args['doc_dispo'] = array_column($this->udManager->getCotesDispo((int)$this->userManager->groupId()), '_id');    
    $args['titre_page'] = 'Résultat de la recherche';
    $args['filtres'] = $filtres;
    $args['result'] = $result;
    $args['search'] = htmlspecialchars($requete);
    $args['scripts_js'] = array('infofonds-sorttable','switchfacettes','switchandsubmit');

    $args['nav_ud'] = $this->renderer->fetch('facettes.phtml', $args);
    $args['pagination'] = $this->renderer->fetch('pagination.phtml', $args);
    if($result->response->numFound > 0) $args['article'] = $this->renderer->fetch($template, $args); // si il y a pas de résultat cela est indiqué dans la partie de facettes.phtml

    return $this->renderer->render($response, 'cadre.phtml', $args);
});

$app->get('/listfonds', function (Request $request, Response $response, array $args) {
    $args['niveau_piece'] = false;
    $args['listfonds'] = $this->udManager->getFonds();
    $args['titre_head'] = 'FJME - Archives - Liste des fonds';
    $args['article'] = $this->renderer->fetch('list_fonds.phtml', $args);
    $args['scripts_js'] = array('infofonds-sorttable', 'filtre-uds');

    return $this->renderer->render($response, 'cadre.phtml', $args);
});

$app->get('/ouvrage/id/{id}', function (Request $request, Response $response, array $args) {

    $args['livre'] = $this->livreManager->getLivre($args['id']);
    if ($args['livre'] === false) throw new \Slim\Exception\NotFoundException($request, $response);

    $args['niveau_piece'] = true;
    $args['titre_head'] = $args['livre']->titre;
    $args['nav_ud'] = '<div class="container w3-gris-clair"><div class="w3-bar"><div class="w3-bar-item">Livre</div></div></div>';
    $args['article'] = $this->renderer->fetch('info_livre.phtml', $args);

    return $this->renderer->render($response, 'cadre.phtml', $args);
});

$app->get('/key', function (Request $request, Response $response, array $args) {
    $k = new Lib\UserManager(null,null);

    echo $k->randomKey(40);
});

$app->get('/acces/{key}', function (Request $request, Response $response, array $args) {

    // on commence par définir que la clef est mauvaise
    $args['article'] = $this->renderer->fetch('login_fail.phtml', $args);

    // si la clef n'est pas alphanumérique on sort
    if(!ctype_alnum($args['key'])) {
        return $this->renderer->render($response, 'cadre.phtml', $args);
    }

    // si la clef est périmée on sort
    if(!$this->userManager->checkKey($args['key'])){
        return $this->renderer->render($response, 'cadre.phtml', $args);
    }

    // autrement on génère la page de bienvenue
    $this->userManager->genCookies($args['key']);

    # FIXME : mettre ces textes dans une varibale des settings
    $args['texte'][400] = "<p>Vous avez maintenant accès à la majorité de nos fonds numérisés. Toutefois, pour des raisons de droits, certains documents, comme le fonds Jacques Delors, ne peuvent être consultés qu'en les murs de notre institution.<p>";
    $args['texte'][600] = "<p>Vous avez maintenant accès à l'entier de nos fonds numérisés sauf ceux soumis à des délais de protection, naturellement.</p>";
    $args['texte'][800] = "<p>Vous êtes employé par la Fondation Jean Monnet, chanceux que vous êtes ! Vous avez accès à tous les documents !</p>";
    $args['texte'][801] = "<p>Il va de soit que vous avez accès à tous les documents, bonne lecture !</p>";
    $args['texte'][1000] = "<p>Vous êtes employé par la Fondation Jean Monnet, chanceux que vous êtes ! Vous avez accès à tous les documents !</p>";

    $args['niv_acces'] = $this->userManager->groupId($args['key']);
    $args['user'] =  $this->userManager->userInfo($args['key']);
    $args['article'] = $this->renderer->fetch('login_ok.phtml', $args);

    return $this->renderer->render($response, 'cadre.phtml', $args);


});

$app->get('/404', function (Request $request, Response $response, array $args) {

    return $this->renderer->render($response, '404.phtml', $args);
});
