<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
            'template_variables' => ['base_url' => $_ENV['BASE_URL']], // si le site est à la racine du serveur web, on met une string vide
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // Mongo settings
        'mongodb' => [
          'uri' => 'mongodb://'.$_ENV['MONGO_USER'].':'.$_ENV['MONGO_PWD'].'@'.$_ENV['MONGO_HOST'].':'.$_ENV['MONGO_PORT'].'/'.$_ENV['MONGO_DB'],
          'ns_uds' => $_ENV['MONGO_DB'].'.uds',
          'ns_users' => $_ENV['MONGO_DB'].'.usagers',
          'ns_livres' => $_ENV['MONGO_DB'].'.biblio',
        ],

        // solR settings
        'solr' => [
            'hostname' => $_ENV['SOLR_HOST'],
            'port'     => $_ENV['SOLR_PORT'],
            'path'     => $_ENV['SOLR_PATH'],
        ],
    ],
];
